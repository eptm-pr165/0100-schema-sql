--
-- PostgreSQL database dump
--

-- Dumped from database version 15.3
-- Dumped by pg_dump version 15.3

-- Started on 2024-04-15 15:32:36

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 214 (class 1259 OID 16399)
-- Name: Chien; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Chien" (
    proprietaire_id uuid NOT NULL,
    race_id uuid NOT NULL,
    identification_amicus text NOT NULL,
    nom text NOT NULL,
    date_naissance timestamp without time zone,
    male boolean NOT NULL,
    id uuid DEFAULT gen_random_uuid()
);


ALTER TABLE public."Chien" OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 16405)
-- Name: Cours; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Cours" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    niveau_id uuid NOT NULL,
    type_id uuid NOT NULL,
    tarif integer NOT NULL
);


ALTER TABLE public."Cours" OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 16409)
-- Name: Ecole; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Ecole" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    nom text NOT NULL,
    adresse text NOT NULL,
    localite_id uuid NOT NULL
);


ALTER TABLE public."Ecole" OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 16415)
-- Name: Localite; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Localite" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    npa integer,
    localite text
);


ALTER TABLE public."Localite" OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 16421)
-- Name: Mail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Mail" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    type text,
    mail text NOT NULL
);


ALTER TABLE public."Mail" OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 16427)
-- Name: Mail_Ecole; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Mail_Ecole" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    mail_id uuid NOT NULL,
    ecole_id uuid NOT NULL
);


ALTER TABLE public."Mail_Ecole" OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 16431)
-- Name: Mail_Moniteur; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Mail_Moniteur" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    mail_id uuid NOT NULL,
    moniteur_id uuid NOT NULL
);


ALTER TABLE public."Mail_Moniteur" OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 16435)
-- Name: Mail_Proprietaire; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Mail_Proprietaire" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    mail_id uuid NOT NULL,
    proprietaire_id uuid NOT NULL
);


ALTER TABLE public."Mail_Proprietaire" OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 16439)
-- Name: Moniteur; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Moniteur" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    civilite text NOT NULL,
    nom text NOT NULL,
    prenom text NOT NULL,
    date_naissance timestamp without time zone,
    adresse text,
    localite_id uuid NOT NULL,
    ecole_id uuid NOT NULL
);


ALTER TABLE public."Moniteur" OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 16445)
-- Name: Niveau; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Niveau" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    niveau text NOT NULL
);


ALTER TABLE public."Niveau" OWNER TO postgres;

--
-- TOC entry 224 (class 1259 OID 16451)
-- Name: Participation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Participation" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    chien_id uuid NOT NULL,
    session_id uuid NOT NULL,
    avec_proprietaire boolean NOT NULL
);


ALTER TABLE public."Participation" OWNER TO postgres;

--
-- TOC entry 225 (class 1259 OID 16455)
-- Name: Proprietaire; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Proprietaire" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    localite_id uuid NOT NULL,
    nom text NOT NULL,
    prenom text NOT NULL,
    date_naissance timestamp without time zone NOT NULL,
    origine text NOT NULL,
    adresse text NOT NULL,
    certificat_delivre timestamp without time zone
);


ALTER TABLE public."Proprietaire" OWNER TO postgres;

--
-- TOC entry 226 (class 1259 OID 16461)
-- Name: Race; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Race" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    nom text NOT NULL
);


ALTER TABLE public."Race" OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 16467)
-- Name: Session; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Session" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    moniteur_id uuid NOT NULL,
    cours_id uuid NOT NULL,
    date timestamp without time zone NOT NULL,
    places integer NOT NULL,
    duree_m integer NOT NULL
);


ALTER TABLE public."Session" OWNER TO postgres;

--
-- TOC entry 228 (class 1259 OID 16471)
-- Name: Telephone; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Telephone" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    type text,
    numero text
);


ALTER TABLE public."Telephone" OWNER TO postgres;

--
-- TOC entry 229 (class 1259 OID 16477)
-- Name: Telephone_Ecole; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Telephone_Ecole" (
    id uuid DEFAULT gen_random_uuid(),
    telephone_id uuid NOT NULL,
    ecole_id uuid NOT NULL
);


ALTER TABLE public."Telephone_Ecole" OWNER TO postgres;

--
-- TOC entry 230 (class 1259 OID 16481)
-- Name: Telephone_Moniteur; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Telephone_Moniteur" (
    id uuid DEFAULT gen_random_uuid(),
    telephone_id uuid NOT NULL,
    moniteur_id uuid NOT NULL
);


ALTER TABLE public."Telephone_Moniteur" OWNER TO postgres;

--
-- TOC entry 231 (class 1259 OID 16485)
-- Name: Telephone_Proprietaire; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Telephone_Proprietaire" (
    id uuid DEFAULT gen_random_uuid(),
    telephone_id uuid NOT NULL,
    proprietaire_id uuid NOT NULL
);


ALTER TABLE public."Telephone_Proprietaire" OWNER TO postgres;

--
-- TOC entry 232 (class 1259 OID 16489)
-- Name: Type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Type" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    type text NOT NULL
);


ALTER TABLE public."Type" OWNER TO postgres;

--
-- TOC entry 3460 (class 0 OID 16399)
-- Dependencies: 214
-- Data for Name: Chien; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Chien" VALUES ('a2af699a-9ece-4236-8741-31df0f680ea2', '713ed198-6b6d-4a76-ba25-385218e21975', '97352', 'Jodelle', '2010-09-05 00:00:00', false, 'c3f25ab2-809c-42c5-97a3-1d1a88303bc5');
INSERT INTO public."Chien" VALUES ('4f119ccf-dc79-4d3c-8c9b-febe0805fb75', '4e5f4319-3e22-40c3-9898-183fff093545', '14778', 'Rex', '2023-08-01 00:00:00', true, '5f16cb3b-0941-4531-9e6f-8ef7172e0c6c');
INSERT INTO public."Chien" VALUES ('4f119ccf-dc79-4d3c-8c9b-febe0805fb75', '82d33a01-6aa3-495c-9eae-e09df64a0976', '15472', 'Adela', '2020-02-10 00:00:00', false, '3ed0075a-b86f-4230-b406-bb4c1237d0cc');


--
-- TOC entry 3461 (class 0 OID 16405)
-- Dependencies: 215
-- Data for Name: Cours; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Cours" VALUES ('2ed5376e-06cb-4d8e-befb-508d2dc21f74', 'f88bb3d4-dbf5-4230-a2c7-771a4dac2137', 'a8e7cc07-0251-4da8-99f3-5901d14ab280', 10);
INSERT INTO public."Cours" VALUES ('6eac60b5-9810-458e-878e-e7df604d45e8', 'f0b720d0-c1d4-49a7-bcde-b4269e9135f3', 'a8e7cc07-0251-4da8-99f3-5901d14ab280', 15);
INSERT INTO public."Cours" VALUES ('c3cc8651-2827-4389-8fd3-f6145bb84d6c', 'e6fc6681-bb5e-4662-8bd3-1b359e5edb4e', 'a8e7cc07-0251-4da8-99f3-5901d14ab280', 20);


--
-- TOC entry 3462 (class 0 OID 16409)
-- Dependencies: 216
-- Data for Name: Ecole; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Ecole" VALUES ('adfb1054-731b-4112-9ecc-150cc54ecab1', 'EPTM - Ecole Professionnelle des Toutous et des Maîtres', ' Chem. Saint-Hubert 2', '91bf96f2-8aaa-4c55-96b4-b6865e20211e');


--
-- TOC entry 3463 (class 0 OID 16415)
-- Dependencies: 217
-- Data for Name: Localite; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Localite" VALUES ('91bf96f2-8aaa-4c55-96b4-b6865e20211e', 1950, 'Sion');


--
-- TOC entry 3464 (class 0 OID 16421)
-- Dependencies: 218
-- Data for Name: Mail; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Mail" VALUES ('173b37a2-8e31-4ee9-a6a2-062afcb1b93e', 'Réception', 'info@eptm.ch');
INSERT INTO public."Mail" VALUES ('36d36cca-c63f-49b7-8da3-300f624cef2a', 'Réservation', 'booking@eptm.ch');
INSERT INTO public."Mail" VALUES ('8d47c06f-98f8-4345-b541-2948ba4b3643', 'Privé', 'camille@hotmail.com');
INSERT INTO public."Mail" VALUES ('5389cded-fbc8-41a0-b38f-de6b324f2172', 'Pro', 'camille.daviaud@eptm.ch');
INSERT INTO public."Mail" VALUES ('b257e101-721e-409e-94bb-db4c79de88f6', 'Pro', 'abel.meraud@eptm.ch');
INSERT INTO public."Mail" VALUES ('b6b6d8e5-2fec-4dbe-8de3-71721e6c82d6', 'Privé', 'denis@hotmail.com');
INSERT INTO public."Mail" VALUES ('ac525586-5250-4736-b799-bdd3856be939', 'Privé', 'claire@hotmail.com');


--
-- TOC entry 3465 (class 0 OID 16427)
-- Dependencies: 219
-- Data for Name: Mail_Ecole; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Mail_Ecole" VALUES ('07cf127f-8541-4922-aeab-650a45bce939', '173b37a2-8e31-4ee9-a6a2-062afcb1b93e', 'adfb1054-731b-4112-9ecc-150cc54ecab1');
INSERT INTO public."Mail_Ecole" VALUES ('16319f84-c06c-4f34-a877-002c7acd6e01', '36d36cca-c63f-49b7-8da3-300f624cef2a', 'adfb1054-731b-4112-9ecc-150cc54ecab1');


--
-- TOC entry 3466 (class 0 OID 16431)
-- Dependencies: 220
-- Data for Name: Mail_Moniteur; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Mail_Moniteur" VALUES ('7df4d7c0-0506-4cdc-a132-600b988d36ef', 'b257e101-721e-409e-94bb-db4c79de88f6', '75e9306d-c172-4f5a-bd6f-a67c0e4a56ab');
INSERT INTO public."Mail_Moniteur" VALUES ('0e272b23-9c31-4bc5-bae4-7792efbfcf72', '5389cded-fbc8-41a0-b38f-de6b324f2172', '882d2b59-c584-4a1a-9aad-08375a9db171');
INSERT INTO public."Mail_Moniteur" VALUES ('25473a50-8510-4ba4-aa24-df7abd06322e', '8d47c06f-98f8-4345-b541-2948ba4b3643', '882d2b59-c584-4a1a-9aad-08375a9db171');


--
-- TOC entry 3467 (class 0 OID 16435)
-- Dependencies: 221
-- Data for Name: Mail_Proprietaire; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Mail_Proprietaire" VALUES ('13af4a10-4f45-4e00-86c2-6cfd89fdcd11', 'ac525586-5250-4736-b799-bdd3856be939', '4f119ccf-dc79-4d3c-8c9b-febe0805fb75');
INSERT INTO public."Mail_Proprietaire" VALUES ('0907f4d2-ebb1-49af-b020-c8d945f1221e', 'b6b6d8e5-2fec-4dbe-8de3-71721e6c82d6', 'a2af699a-9ece-4236-8741-31df0f680ea2');


--
-- TOC entry 3468 (class 0 OID 16439)
-- Dependencies: 222
-- Data for Name: Moniteur; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Moniteur" VALUES ('75e9306d-c172-4f5a-bd6f-a67c0e4a56ab', 'Monsieur', 'Méraud', 'Abel', '1877-04-12 00:00:00', 'Chemin du chemin 12', '91bf96f2-8aaa-4c55-96b4-b6865e20211e', 'adfb1054-731b-4112-9ecc-150cc54ecab1');
INSERT INTO public."Moniteur" VALUES ('882d2b59-c584-4a1a-9aad-08375a9db171', 'Madame', 'Daviau', 'Camille', '1997-06-28 00:00:00', 'Avenue des bouquins 1', '91bf96f2-8aaa-4c55-96b4-b6865e20211e', 'adfb1054-731b-4112-9ecc-150cc54ecab1');


--
-- TOC entry 3469 (class 0 OID 16445)
-- Dependencies: 223
-- Data for Name: Niveau; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Niveau" VALUES ('f0b720d0-c1d4-49a7-bcde-b4269e9135f3', 'Chiot');
INSERT INTO public."Niveau" VALUES ('f88bb3d4-dbf5-4230-a2c7-771a4dac2137', 'Ado');
INSERT INTO public."Niveau" VALUES ('e6fc6681-bb5e-4662-8bd3-1b359e5edb4e', 'Adulte');


--
-- TOC entry 3470 (class 0 OID 16451)
-- Dependencies: 224
-- Data for Name: Participation; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Participation" VALUES ('1ae5f561-a295-436e-b5b4-bc458a5b757e', '5f16cb3b-0941-4531-9e6f-8ef7172e0c6c', 'da3a860c-995d-44f6-844f-4721e5798087', true);
INSERT INTO public."Participation" VALUES ('17a1e52e-c000-4a47-a629-d4b9f0d06393', 'c3f25ab2-809c-42c5-97a3-1d1a88303bc5', 'ad7c7814-cd6f-4795-a850-332f56b702fc', false);
INSERT INTO public."Participation" VALUES ('de7cb073-9e21-46cc-8d3d-d21e351aceed', '3ed0075a-b86f-4230-b406-bb4c1237d0cc', 'ad7c7814-cd6f-4795-a850-332f56b702fc', false);
INSERT INTO public."Participation" VALUES ('934377cd-e804-46b1-813a-37c4c8c3f777', '5f16cb3b-0941-4531-9e6f-8ef7172e0c6c', 'ad7c7814-cd6f-4795-a850-332f56b702fc', false);
INSERT INTO public."Participation" VALUES ('26c376e1-5d07-43c9-a851-327a002e1a1f', 'c3f25ab2-809c-42c5-97a3-1d1a88303bc5', '9fb22302-a917-4f26-8fe5-24ab4bc25808', true);
INSERT INTO public."Participation" VALUES ('5b8fcbff-51e3-4312-b057-ed26adb5a733', '3ed0075a-b86f-4230-b406-bb4c1237d0cc', '9fb22302-a917-4f26-8fe5-24ab4bc25808', true);
INSERT INTO public."Participation" VALUES ('7277c3d5-f1b1-4c5a-b88a-08f84cada7fe', 'c3f25ab2-809c-42c5-97a3-1d1a88303bc5', '5ee63cce-68fb-4ccc-9e09-3c71f5912f50', false);
INSERT INTO public."Participation" VALUES ('4e5df7f9-f2d4-41d1-a8cd-58ad36733278', '5f16cb3b-0941-4531-9e6f-8ef7172e0c6c', '5ee63cce-68fb-4ccc-9e09-3c71f5912f50', false);
INSERT INTO public."Participation" VALUES ('b02b19ac-1232-4e21-bf9e-0373dc957673', 'c3f25ab2-809c-42c5-97a3-1d1a88303bc5', '26f0f123-16ec-4ea9-9ff8-134b6b6143df', true);
INSERT INTO public."Participation" VALUES ('7e8746bd-7cdd-4075-8fb4-2e5f39000b39', '5f16cb3b-0941-4531-9e6f-8ef7172e0c6c', '26f0f123-16ec-4ea9-9ff8-134b6b6143df', true);


--
-- TOC entry 3471 (class 0 OID 16455)
-- Dependencies: 225
-- Data for Name: Proprietaire; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Proprietaire" VALUES ('a2af699a-9ece-4236-8741-31df0f680ea2', '91bf96f2-8aaa-4c55-96b4-b6865e20211e', 'Porcher', 'Denis', '1970-12-04 00:00:00', 'Sion', 'Les mauriette 9', NULL);
INSERT INTO public."Proprietaire" VALUES ('4f119ccf-dc79-4d3c-8c9b-febe0805fb75', '91bf96f2-8aaa-4c55-96b4-b6865e20211e', 'Barre', 'Claire', '2002-01-30 00:00:00', 'Sierre', 'Forêt des arbres 11', '2021-01-01 00:00:00');


--
-- TOC entry 3472 (class 0 OID 16461)
-- Dependencies: 226
-- Data for Name: Race; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Race" VALUES ('7f296c62-d8fe-4dd6-af9d-f41407e8da7e', 'Berger Allemand');
INSERT INTO public."Race" VALUES ('4e5f4319-3e22-40c3-9898-183fff093545', 'Bulldog');
INSERT INTO public."Race" VALUES ('82d33a01-6aa3-495c-9eae-e09df64a0976', 'Labrador');
INSERT INTO public."Race" VALUES ('1723ebd0-5b91-463c-bae3-43f971d258c7', 'Chihuahua');
INSERT INTO public."Race" VALUES ('713ed198-6b6d-4a76-ba25-385218e21975', 'Teckel');


--
-- TOC entry 3473 (class 0 OID 16467)
-- Dependencies: 227
-- Data for Name: Session; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Session" VALUES ('5ee63cce-68fb-4ccc-9e09-3c71f5912f50', '882d2b59-c584-4a1a-9aad-08375a9db171', 'c3cc8651-2827-4389-8fd3-f6145bb84d6c', '2024-04-01 00:00:00', 5, 180);
INSERT INTO public."Session" VALUES ('26f0f123-16ec-4ea9-9ff8-134b6b6143df', '882d2b59-c584-4a1a-9aad-08375a9db171', '6eac60b5-9810-458e-878e-e7df604d45e8', '2024-03-12 00:00:00', 5, 180);
INSERT INTO public."Session" VALUES ('da3a860c-995d-44f6-844f-4721e5798087', '882d2b59-c584-4a1a-9aad-08375a9db171', '6eac60b5-9810-458e-878e-e7df604d45e8', '2024-04-05 00:00:00', 5, 180);
INSERT INTO public."Session" VALUES ('ad7c7814-cd6f-4795-a850-332f56b702fc', '75e9306d-c172-4f5a-bd6f-a67c0e4a56ab', '2ed5376e-06cb-4d8e-befb-508d2dc21f74', '2024-01-01 00:00:00', 5, 180);
INSERT INTO public."Session" VALUES ('9fb22302-a917-4f26-8fe5-24ab4bc25808', '75e9306d-c172-4f5a-bd6f-a67c0e4a56ab', '2ed5376e-06cb-4d8e-befb-508d2dc21f74', '2024-02-15 00:00:00', 5, 180);


--
-- TOC entry 3474 (class 0 OID 16471)
-- Dependencies: 228
-- Data for Name: Telephone; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Telephone" VALUES ('031ab264-2fec-4993-bcdf-4e40cdaab9d2', 'Mobile', '+41 78 901 74 29');
INSERT INTO public."Telephone" VALUES ('664e99be-51a6-46e7-ba88-2ba8883fbe2e', 'Mobile', '+41 77 289 22 93');
INSERT INTO public."Telephone" VALUES ('157792b4-11e5-4c5a-851b-c69e85aa0bd0', 'Mobile', '+41 79 255 67 83');
INSERT INTO public."Telephone" VALUES ('2875d084-b303-4232-b171-7b4cae133ee7', 'Pro', '+41 52 713 18 69');
INSERT INTO public."Telephone" VALUES ('d663b299-5443-4b26-bc8c-a3ff8b3df9ea', 'Mobile', '+41 76 573 28 59');
INSERT INTO public."Telephone" VALUES ('1eaeb30c-c63e-4722-b5ed-71fbc29d122c', 'Pro', '+41 52 879 62 10');
INSERT INTO public."Telephone" VALUES ('4f4725d7-57a8-46fe-bea5-1565967dc2b9', 'Fax', '+41 44 909 54 19');
INSERT INTO public."Telephone" VALUES ('192409ce-3288-4244-9629-de205b1716db', 'Réception', '+41 44 909 54 18');


--
-- TOC entry 3475 (class 0 OID 16477)
-- Dependencies: 229
-- Data for Name: Telephone_Ecole; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Telephone_Ecole" VALUES ('11014e08-fd7e-445f-9e34-837a3857f263', '192409ce-3288-4244-9629-de205b1716db', 'adfb1054-731b-4112-9ecc-150cc54ecab1');
INSERT INTO public."Telephone_Ecole" VALUES ('68c0cea7-5bfc-4853-b850-9419f5ce2b3c', '4f4725d7-57a8-46fe-bea5-1565967dc2b9', 'adfb1054-731b-4112-9ecc-150cc54ecab1');


--
-- TOC entry 3476 (class 0 OID 16481)
-- Dependencies: 230
-- Data for Name: Telephone_Moniteur; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Telephone_Moniteur" VALUES ('c881a016-c779-48f4-8ad9-b04734e3149c', '1eaeb30c-c63e-4722-b5ed-71fbc29d122c', '75e9306d-c172-4f5a-bd6f-a67c0e4a56ab');
INSERT INTO public."Telephone_Moniteur" VALUES ('9a0c83ba-f09f-4041-983a-48418127be50', 'd663b299-5443-4b26-bc8c-a3ff8b3df9ea', '75e9306d-c172-4f5a-bd6f-a67c0e4a56ab');
INSERT INTO public."Telephone_Moniteur" VALUES ('0e8ecfbd-1269-4afc-8ef9-74b6235d16af', '2875d084-b303-4232-b171-7b4cae133ee7', '882d2b59-c584-4a1a-9aad-08375a9db171');
INSERT INTO public."Telephone_Moniteur" VALUES ('908f41f3-b2de-4970-a41a-b8a4f2f54ae1', '157792b4-11e5-4c5a-851b-c69e85aa0bd0', '882d2b59-c584-4a1a-9aad-08375a9db171');


--
-- TOC entry 3477 (class 0 OID 16485)
-- Dependencies: 231
-- Data for Name: Telephone_Proprietaire; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Telephone_Proprietaire" VALUES ('327a96e9-d8b6-41c7-b2ac-e4e5436783ff', '664e99be-51a6-46e7-ba88-2ba8883fbe2e', '4f119ccf-dc79-4d3c-8c9b-febe0805fb75');
INSERT INTO public."Telephone_Proprietaire" VALUES ('09a55d37-983f-46df-82ba-5da9f28ddc99', '031ab264-2fec-4993-bcdf-4e40cdaab9d2', 'a2af699a-9ece-4236-8741-31df0f680ea2');


--
-- TOC entry 3478 (class 0 OID 16489)
-- Dependencies: 232
-- Data for Name: Type; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Type" VALUES ('a8e7cc07-0251-4da8-99f3-5901d14ab280', 'Groupe');
INSERT INTO public."Type" VALUES ('cb87f864-7838-4522-9490-f99da749c65c', 'Privé');
INSERT INTO public."Type" VALUES ('a74848ac-2bc4-45bf-a0e3-090f126bb68a', ' A domicile');


--
-- TOC entry 3264 (class 2606 OID 16496)
-- Name: Chien Chien_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Chien"
    ADD CONSTRAINT "Chien_pkey" PRIMARY KEY (id);


--
-- TOC entry 3266 (class 2606 OID 16498)
-- Name: Cours Cours_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Cours"
    ADD CONSTRAINT "Cours_pkey" PRIMARY KEY (id);


--
-- TOC entry 3268 (class 2606 OID 16500)
-- Name: Ecole Ecole_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Ecole"
    ADD CONSTRAINT "Ecole_pkey" PRIMARY KEY (id);


--
-- TOC entry 3270 (class 2606 OID 16502)
-- Name: Localite Localite_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Localite"
    ADD CONSTRAINT "Localite_pkey" PRIMARY KEY (id);


--
-- TOC entry 3274 (class 2606 OID 16504)
-- Name: Mail_Ecole Mail_Ecole_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Mail_Ecole"
    ADD CONSTRAINT "Mail_Ecole_pkey" PRIMARY KEY (id);


--
-- TOC entry 3276 (class 2606 OID 16506)
-- Name: Mail_Moniteur Mail_Moniteur_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Mail_Moniteur"
    ADD CONSTRAINT "Mail_Moniteur_pkey" PRIMARY KEY (id);


--
-- TOC entry 3278 (class 2606 OID 16508)
-- Name: Mail_Proprietaire Mail_Proprietaire_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Mail_Proprietaire"
    ADD CONSTRAINT "Mail_Proprietaire_pkey" PRIMARY KEY (id);


--
-- TOC entry 3272 (class 2606 OID 16510)
-- Name: Mail Mail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Mail"
    ADD CONSTRAINT "Mail_pkey" PRIMARY KEY (id);


--
-- TOC entry 3280 (class 2606 OID 16512)
-- Name: Moniteur Moniteur_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Moniteur"
    ADD CONSTRAINT "Moniteur_pkey" PRIMARY KEY (id);


--
-- TOC entry 3282 (class 2606 OID 16514)
-- Name: Niveau Niveau_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Niveau"
    ADD CONSTRAINT "Niveau_pkey" PRIMARY KEY (id);


--
-- TOC entry 3284 (class 2606 OID 16516)
-- Name: Participation Participation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Participation"
    ADD CONSTRAINT "Participation_pkey" PRIMARY KEY (id);


--
-- TOC entry 3286 (class 2606 OID 16518)
-- Name: Proprietaire Proprietaire_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proprietaire"
    ADD CONSTRAINT "Proprietaire_pkey" PRIMARY KEY (id);


--
-- TOC entry 3288 (class 2606 OID 16520)
-- Name: Race Race_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Race"
    ADD CONSTRAINT "Race_pkey" PRIMARY KEY (id);


--
-- TOC entry 3290 (class 2606 OID 16522)
-- Name: Session Session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Session"
    ADD CONSTRAINT "Session_pkey" PRIMARY KEY (id);


--
-- TOC entry 3292 (class 2606 OID 16524)
-- Name: Telephone Telephone_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Telephone"
    ADD CONSTRAINT "Telephone_pkey" PRIMARY KEY (id);


--
-- TOC entry 3294 (class 2606 OID 16526)
-- Name: Type Type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Type"
    ADD CONSTRAINT "Type_pkey" PRIMARY KEY (id);


--
-- TOC entry 3310 (class 2606 OID 16527)
-- Name: Session Cours; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Session"
    ADD CONSTRAINT "Cours" FOREIGN KEY (cours_id) REFERENCES public."Cours"(id);


--
-- TOC entry 3306 (class 2606 OID 16532)
-- Name: Moniteur Ecole; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Moniteur"
    ADD CONSTRAINT "Ecole" FOREIGN KEY (ecole_id) REFERENCES public."Ecole"(id);


--
-- TOC entry 3300 (class 2606 OID 16537)
-- Name: Mail_Ecole Ecole; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Mail_Ecole"
    ADD CONSTRAINT "Ecole" FOREIGN KEY (ecole_id) REFERENCES public."Ecole"(id);


--
-- TOC entry 3312 (class 2606 OID 16542)
-- Name: Telephone_Ecole Ecole; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Telephone_Ecole"
    ADD CONSTRAINT "Ecole" FOREIGN KEY (ecole_id) REFERENCES public."Ecole"(id);


--
-- TOC entry 3309 (class 2606 OID 16547)
-- Name: Proprietaire Localite; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proprietaire"
    ADD CONSTRAINT "Localite" FOREIGN KEY (localite_id) REFERENCES public."Localite"(id);


--
-- TOC entry 3299 (class 2606 OID 16552)
-- Name: Ecole Localite; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Ecole"
    ADD CONSTRAINT "Localite" FOREIGN KEY (localite_id) REFERENCES public."Localite"(id);


--
-- TOC entry 3307 (class 2606 OID 16557)
-- Name: Moniteur Localite; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Moniteur"
    ADD CONSTRAINT "Localite" FOREIGN KEY (localite_id) REFERENCES public."Localite"(id);


--
-- TOC entry 3304 (class 2606 OID 16562)
-- Name: Mail_Proprietaire Mail; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Mail_Proprietaire"
    ADD CONSTRAINT "Mail" FOREIGN KEY (mail_id) REFERENCES public."Mail"(id);


--
-- TOC entry 3301 (class 2606 OID 16567)
-- Name: Mail_Ecole Mail; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Mail_Ecole"
    ADD CONSTRAINT "Mail" FOREIGN KEY (mail_id) REFERENCES public."Mail"(id);


--
-- TOC entry 3302 (class 2606 OID 16572)
-- Name: Mail_Moniteur Mail; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Mail_Moniteur"
    ADD CONSTRAINT "Mail" FOREIGN KEY (mail_id) REFERENCES public."Mail"(id);


--
-- TOC entry 3303 (class 2606 OID 16577)
-- Name: Mail_Moniteur Moniteur; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Mail_Moniteur"
    ADD CONSTRAINT "Moniteur" FOREIGN KEY (moniteur_id) REFERENCES public."Moniteur"(id);


--
-- TOC entry 3314 (class 2606 OID 16582)
-- Name: Telephone_Moniteur Moniteur; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Telephone_Moniteur"
    ADD CONSTRAINT "Moniteur" FOREIGN KEY (moniteur_id) REFERENCES public."Moniteur"(id);


--
-- TOC entry 3311 (class 2606 OID 16587)
-- Name: Session Moniteur; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Session"
    ADD CONSTRAINT "Moniteur" FOREIGN KEY (moniteur_id) REFERENCES public."Moniteur"(id);


--
-- TOC entry 3297 (class 2606 OID 16592)
-- Name: Cours Niveau; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Cours"
    ADD CONSTRAINT "Niveau" FOREIGN KEY (niveau_id) REFERENCES public."Niveau"(id);


--
-- TOC entry 3305 (class 2606 OID 16597)
-- Name: Mail_Proprietaire Proprietaire; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Mail_Proprietaire"
    ADD CONSTRAINT "Proprietaire" FOREIGN KEY (proprietaire_id) REFERENCES public."Proprietaire"(id);


--
-- TOC entry 3316 (class 2606 OID 16602)
-- Name: Telephone_Proprietaire Proprietaire; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Telephone_Proprietaire"
    ADD CONSTRAINT "Proprietaire" FOREIGN KEY (proprietaire_id) REFERENCES public."Proprietaire"(id);


--
-- TOC entry 3295 (class 2606 OID 16607)
-- Name: Chien Proprietaire; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Chien"
    ADD CONSTRAINT "Proprietaire" FOREIGN KEY (proprietaire_id) REFERENCES public."Proprietaire"(id);


--
-- TOC entry 3296 (class 2606 OID 16612)
-- Name: Chien Race; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Chien"
    ADD CONSTRAINT "Race" FOREIGN KEY (race_id) REFERENCES public."Race"(id);


--
-- TOC entry 3308 (class 2606 OID 16617)
-- Name: Participation Session; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Participation"
    ADD CONSTRAINT "Session" FOREIGN KEY (session_id) REFERENCES public."Session"(id);
	

--
-- Correction manuelle
-- Name: Participation Session; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Participation"
    ADD CONSTRAINT "Chien" FOREIGN KEY (chien_id) REFERENCES public."Chien"(id);


--
-- TOC entry 3317 (class 2606 OID 16622)
-- Name: Telephone_Proprietaire Telephone; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Telephone_Proprietaire"
    ADD CONSTRAINT "Telephone" FOREIGN KEY (telephone_id) REFERENCES public."Telephone"(id);


--
-- TOC entry 3313 (class 2606 OID 16627)
-- Name: Telephone_Ecole Telephone; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Telephone_Ecole"
    ADD CONSTRAINT "Telephone" FOREIGN KEY (telephone_id) REFERENCES public."Telephone"(id);


--
-- TOC entry 3315 (class 2606 OID 16632)
-- Name: Telephone_Moniteur Telephone; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Telephone_Moniteur"
    ADD CONSTRAINT "Telephone" FOREIGN KEY (telephone_id) REFERENCES public."Telephone"(id);


--
-- TOC entry 3298 (class 2606 OID 16637)
-- Name: Cours Type; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Cours"
    ADD CONSTRAINT "Type" FOREIGN KEY (type_id) REFERENCES public."Type"(id);


-- Completed on 2024-04-15 15:32:37

--
-- PostgreSQL database dump complete
--

